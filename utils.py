import logging
import os
import psycopg2
import pygrametl
import requests
import shutil
import yaml

from datetime import datetime
from pygrametl.datasources import CSVSource
from pygrametl.datasources import HashJoiningSource
from pygrametl.datasources import SQLSource
from pygrametl.steps import RenamingFromToStep
from pygrametl.steps import Step
from pygrametl.tables import Dimension

logging.basicConfig(
    filename="log/testimonial.log",
    filemode="w",
    level=logging.ERROR)


def loadConfig():
    """
    Load config data from config.yml
    """
    with open("config.yml") as f:
        loaded_config = yaml.load(f)
        return loaded_config

config = loadConfig()


def addCreationTime(row):
    row["creation_date"] = datetime.now()


def addPlaceholderPhotoID(row):
    row["photo_id"] = None


def pick(row, keys):
    return {key: row[key] for key in keys}


def assignDefaultPub(row):
    try:
        if not row["is_published"]:
            row["is_published"] = "0"
    except Exception as e:
        row["is_published"] = "0"


def assignDefaultFeature(row):
    try:
        if not row["is_featured"]:
            row["is_featured"] = "0"
    except Exception as e:
        row["is_featured"] = "0"


"""
 A STEP OR pygrametl.setdefaults(row, ["creation_date"], defaults=[pygrametl.now()])
"""
default_feature = Step(
    worker=assignDefaultFeature,
    next=None,
    name="assignDefaultFeature")

default_pub = Step(
    worker=assignDefaultPub,
    next="assignDefaultFeature",
    name="assignDefaultPub")

add_photo_id = Step(
    worker=addPlaceholderPhotoID,
    next="assignDefaultPub",
    name="addPhotoID")

add_creation_time = Step(
    worker=addCreationTime,
    next="addPhotoID",
    name="addtime")

rename_apprv_pub = RenamingFromToStep(
    {"is_approved": "is_published", }, next="addtime", name="rename")


def assignDefaultBody(row):
    try:
        if not row["body"]:
            row["body"] = ""
    except Exception as e:
        row["body"] = ""


def assignDefaultSubject(row):
    try:
        if not row["subject"]:
            row["subject"] = ""
    except Exception as e:
        row["subject"] = ""


def addLanguageCode(row):
    row["language_code"] = "en"


def assignDefaultDN(row):
    try:
        if not row["display_name"]:
            row["display_name"] = ""
    except Exception as e:
        row["display_name"] = ""


default_sub = Step(worker=assignDefaultDN, next=None, name="assignDefaultDN")

default_sub = Step(
    worker=assignDefaultSubject,
    next="assignDefaultDN",
    name="assignDefaultSubject")

default_body = Step(
    worker=assignDefaultBody,
    next="assignDefaultSubject",
    name="assignDefaultBody")

add_language_code = Step(
    worker=addLanguageCode,
    next="assignDefaultBody",
    name="addlanguagecode")

rename_signed_by_dn = RenamingFromToStep(
    {"signed_by": "display_name", }, next="addlanguagecode", name="rename_c_dn")

rename_content_body = RenamingFromToStep(
    {"content": "body", }, next="rename_c_dn", name="rename_c_b")

combined_source_images = []

dw_string = config["database"]
dw_pgconn = psycopg2.connect(dw_string)
dw_conn_wrapper = pygrametl.ConnectionWrapper(connection=dw_pgconn)

testimonial_source = CSVSource(open("data/t.tsv", "r", 16384), delimiter="\t")
testimonial_images = CSVSource(
    open(
        "data/t_images.tsv",
        "r",
        16384),
    delimiter="\t")

uploaded_images = CSVSource(
    open(
        "data/u_images.tsv",
        "r",
        16384),
    delimiter="\t")

combined_source_1 = HashJoiningSource(
    src1=testimonial_source,
    key1="testimonial_id",
    src2=testimonial_images,
    key2="testimonial_id")


testimonials_trans__dimension = Dimension(
    name=config["testimonials_trans_tbl"],
    key="id",
    attributes=config["testimonial_trans_check"])

testimonials_trans__dimension_insert = Dimension(
    name=config["testimonials_trans_tbl"],
    key="id",
    attributes=config["testimonial_trans_atts"])

testimonial_dimension = Dimension(
    name=config["testimonials_tbl"],
    key="id",
    attributes=config["testimonial_atts"])

filer_image = Dimension(
    name=config["filer_image_name"],
    key="file_ptr_id",
    attributes=config["filer_image_atts"])

filer_file = Dimension(
    name=config["filer_file_name"],
    key="id",
    attributes=config["filer_file_atts"])
