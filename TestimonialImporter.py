#!/usr/bin/python
# -*- coding: utf-8 -*-

from utils import *


def emptyTables():

    query_target = "TRUNCATE filer_image"
    dw_conn_wrapper.execute(query_target)
    filer_file = "TRUNCATE filer_file"
    dw_conn_wrapper.execute(filer_file)
    testimonial_trn = "TRUNCATE kevlar_testimonials_testimonial_translation"
    dw_conn_wrapper.execute(testimonial_trn)
    testimonial = "TRUNCATE kevlar_testimonials_testimonial"
    dw_conn_wrapper.execute(testimonial)


def extractImagesForTestimonial(image_id):

    global combined_source_images
    for image_row in combined_source_images:
        if image_row["image_id"] == str(image_id):
            return image_row
    return {}


def loadImages():
    """
    uploaded_images CSVSource
    """
    global combined_source_images
    for image in uploaded_images:
        combined_source_images.append(image)


def copy_default(src, dest, buffer_size=16000):

    with open(src, "rb") as fsrc:
        with open(dest, "wb") as fdest:
            shutil.copyfileobj(fsrc, fdest, buffer_size)


def processRow():

    counter = 1
    for row in combined_source_1:
        if counter <= config["NUMBER_OF_IMAGES_TO_PROCESS"]:
            image_id = row["image_id"]
            related_image = extractImagesForTestimonial(image_id)
            orig_filename = related_image["original_filename"]
            image_hash = related_image["hash"]

            first, second = image_hash[:2], image_hash[2:4]
            image_url = "%s%s/%s/%s.%s" % (config["url"],
                                           first,
                                           second,
                                           image_hash,
                                           related_image["extension"])
            filename = "%s.%s" % (image_hash, related_image["extension"])
            FILE_DIR = "%s%s/%s/" % (config["MEDIA_PATH"], first, second)
            FILE_PATH = "%s%s/%s/%s" % (config["MEDIA_PATH"],
                                        first, second, filename)
            DB_PATH = "%s%s/%s/%s" % (config["FILER_PUBLIC"],
                                      first, second, filename)
            r = requests.get(image_url, stream=True)

            if r.status_code == 200:
                if not os.path.exists(FILE_DIR):
                    os.makedirs(FILE_DIR)

                with open(FILE_PATH, "wb") as f:
                    r.raw.decode_content = True
                    shutil.copyfileobj(r.raw, f)
            else:
                # todo add a default image if there is no image
                logging.error("Image error %s", r.status_code)
                print("Image non existant ", image_url, "\n")
                DB_PATH = "%s%s" % (config["FILER_PUBLIC"], config[
                                    "DEFAULT_IMG_NAME"])
                FILE_PATH = "%s%s" % (config["MEDIA_PATH"], config[
                                      "DEFAULT_IMG_NAME"])
                print(os.path.isfile(FILE_PATH), FILE_PATH)
                if not os.path.isfile(FILE_PATH):
                    copy_default(config["PATH_TO_DEFAULT_IMAGE"], FILE_PATH)

            child_row = row
            rename_apprv_pub.process(row)
            orig_testimonial_id = row["testimonial_id"]
            new_row = pick(row, config["testimonial_atts"])

            rename_content_body.process(child_row)
            child_row = pick(child_row, config["testimonial_trans_check"])
            test_id = testimonials_trans__dimension.lookup(child_row)
            if test_id:
                print("THE TRANS ID", test_id, child_row, "\n")
                logging.error(
                    "Continuing to next row since we already have a testimonial %s" %
                    (orig_testimonial_id))
                print(
                    "Continuing next row testimonial FILE ID:%s IN_DB:%s \n" %
                    (orig_testimonial_id, test_id))
                counter += 1
                continue

            new_row["is_featured"] = "1"
            print("NEW ROW", new_row, "\n")
            testimonial_id = testimonial_dimension.insert(new_row)

            child_row["master_id"] = testimonial_id
            child_row = pick(child_row, config["testimonial_trans_atts"])
            print("Trans", child_row, "\n")
            testimonials_trans__dimension_insert.ensure(child_row)

            # insert the filer image
            filer_row = {
                "must_always_publish_author_credit": False,
                "must_always_publish_copyright": False}
            print("IMAGE", filer_row, "\n")
            file_ptr_id = filer_image.insert(filer_row)

            # creates the filer image and updates the photo_id in the testimonial table
            row.update({"photo_id": file_ptr_id,
                        "id": testimonial_id, "is_featured": "1"})
            testimonial_dimension.update(row)

            """ CREATE THE FILE FILE OBJECT"""
            filer_file_row = {
                "file": DB_PATH,
                "has_all_mandatory_data": True,
                "is_public": True,
                "sha1": image_hash,
                "modified_at": datetime.now(),
                "uploaded_at": datetime.now(),
                "polymorphic_ctype_id": 30,
                "original_filename": orig_filename,
                "name": orig_filename,
                "id": file_ptr_id}

            print("FILE", filer_file_row, "\n")
            filer_file.insert(filer_file_row)
            print(
                "TESTIMONIAL ID FROM FILE ",
                orig_testimonial_id,
                "TESTIMONIAL_SYS_GEN",
                testimonial_id,
                "\n")
            print(
                "\n",
                row,
                related_image,
                "\n",
                filer_row,
                "\n",
                filer_file_row,
                "\n")

        counter += 1

if __name__ == "__main__":
    loadImages()
    processRow()
    dw_conn_wrapper.commit()
    dw_conn_wrapper.close()
