import hashlib


def generate_sha1(raw):

    sha = hashlib.sha1()
    raw.seek(0)

    while True:
        buf = raw.read(104857600)
        if not buf:
            break
        sha.update(buf)

    sha1 = sha.hexdigest()
    raw.seek(0)
    return sha1
