CREATE TABLE kevlar_testimonials_testimonial_translation (
    id integer NOT NULL,
    language_code character varying(15) NOT NULL,
    subject character varying(255) NOT NULL,
    body text NOT NULL,
    display_name character varying(255) NOT NULL,
    master_id integer -- connects to the id field in kevlar_testimonials_testimonial
);

Table "public.kevlar_testimonials_testimonial_translation"
    Column     |          Type          |                                        Modifiers                                         
---------------+------------------------+------------------------------------------------------------------------------------------
 id            | integer                | not null default nextval('kevlar_testimonials_testimonial_translation_id_seq'::regclass)
 language_code | character varying(15)  | not null
 subject       | character varying(255) | not null
 body          | text                   | not null
 display_name  | character varying(255) | not null
 master_id     | integer                | 
Indexes:
    "kevlar_testimonials_testimonial_translation_pkey" PRIMARY KEY, btree (id)
    "kevlar_testimonials_testimo_language_code_19b25389df1c7db2_uniq" UNIQUE CONSTRAINT, btree (language_code, master_id)
    "kevlar_testimonials_testimonial_translation_language_code" btree (language_code)
    "kevlar_testimonials_testimonial_translation_language_code_like" btree (language_code varchar_pattern_ops)
    "kevlar_testimonials_testimonial_translation_master_id" btree (master_id)
Foreign-key constraints:
    "master_id_refs_id_359a024f" FOREIGN KEY (master_id) REFERENCES 
    										kevlar_testimonials_testimonial(id) DEFERRABLE INITIALLY DEFERRE

========================================================================================================================
CREATE TABLE kevlar_testimonials_testimonial (
    id integer NOT NULL, -- connects to the master_id field in kevlar_testimonials_testimonial_translation
    photo_id integer,
    is_published boolean NOT NULL,
    is_featured boolean NOT NULL,
    creation_date timestamp with time zone NOT NULL
);

                                     Table "public.kevlar_testimonials_testimonial"
    Column     |           Type           |                                  Modifiers                                   
---------------+--------------------------+------------------------------------------------------------------------------
 id            | integer                  | not null default nextval('kevlar_testimonials_testimonial_id_seq'::regclass)
 photo_id      | integer                  | 
 is_published  | boolean                  | not null
 is_featured   | boolean                  | not null
 creation_date | timestamp with time zone | not null
Indexes:
    "kevlar_testimonials_testimonial_pkey" PRIMARY KEY, btree (id)
    "kevlar_testimonials_testimonial_photo_id" btree (photo_id)
Foreign-key constraints:
    "photo_id_refs_file_ptr_id_b4bcecfe" FOREIGN KEY (photo_id) REFERENCES 

    filer_image(file_ptr_id) DEFERRABLE INITIALLY DEFERRED
Referenced by:
    TABLE "kevlar_testimonials_testimonial_translation" CONSTRAINT "master_id_refs_id_359a024f" FOREIGN KEY (master_id) REFERENCES kevlar_testimonials_testimonial(id) DEFERRABLE INITIALLY DEFERRED

'photo_id from kevlar_testimonials_testimonial connects to filer_image which has file_ptr_id and filer_image connects to 
filer_file which has id the same as file_ptr_id ' 

1. TABLE "kevlar_testimonials_testimonial" CONSTRAINT "photo_id_refs_file_ptr_id_b4bcecfe" FOREIGN KEY (photo_id) 
                REFERENCES filer_image(file_ptr_id) DEFERRABLE INITIALLY DEFERRED

2. "file_ptr_id_refs_id_7375d00c" FOREIGN KEY (file_ptr_id) REFERENCES filer_file(id) DEFERRABLE INITIALLY DEFERRED

    ===================================================================================================================
db=# \d filer_image;
                        Table "public.filer_image"
              Column               |           Type           | Modifiers 
-----------------------------------+--------------------------+-----------
 file_ptr_id                       | integer                  | not null
 _height                           | integer                  | 
 _width                            | integer                  | 
 date_taken                        | timestamp with time zone | 
 default_alt_text                  | character varying(255)   | 
 default_caption                   | character varying(255)   | 
 author                            | character varying(255)   | 
 must_always_publish_author_credit | boolean                  | not null
 must_always_publish_copyright     | boolean                  | not null
 subject_location                  | character varying(64)    | 
Indexes:
    "filer_image_pkey" PRIMARY KEY, btree (file_ptr_id)
Foreign-key constraints:
    
    "file_ptr_id_refs_id_7375d00c" FOREIGN KEY (file_ptr_id) REFERENCES filer_file(id) DEFERRABLE INITIALLY DEFERRED

Referenced by:
    TABLE "aldryn_bootstrap3_boostrap3imageplugin" CONSTRAINT "file_id_refs_file_ptr_id_2f703071" FOREIGN KEY (file_id) REFERENCES filer_image(file_ptr_id) DEFERRABLE INITIALLY DEFERRED
    TABLE "aldryn_bootstrap3_bootstrap3carouselslideplugin" CONSTRAINT "image_id_refs_file_ptr_id_93ad39ea" FOREIGN KEY (image_id) REFERENCES filer_image(file_ptr_id) DEFERRABLE INITIALLY DEFERRED
    TABLE "cmsplugin_filer_image_filerimage" CONSTRAINT "image_id_refs_file_ptr_id_affb196b" FOREIGN KEY (image_id) REFERENCES filer_image(file_ptr_id) DEFERRABLE INITIALLY DEFERRED
    TABLE "djangocms_gmaps_location" CONSTRAINT "marker_icon_id_refs_file_ptr_id_346b11fe" FOREIGN KEY (marker_icon_id) REFERENCES filer_image(file_ptr_id) DEFERRABLE INITIALLY DEFERRED
    TABLE "kevlar_available_pets_petphoto" CONSTRAINT "photo_id_refs_file_ptr_id_2bffa61e" FOREIGN KEY (photo_id) REFERENCES filer_image(file_ptr_id) DEFERRABLE INITIALLY DEFERRED
    TABLE "djangocms_gmaps_location" CONSTRAINT "photo_id_refs_file_ptr_id_346b11fe" FOREIGN KEY (photo_id) REFERENCES filer_image(file_ptr_id) DEFERRABLE INITIALLY DEFERRED
    TABLE "kevlar_available_pets_breedphoto" CONSTRAINT "photo_id_refs_file_ptr_id_3cf5a31a" FOREIGN KEY (photo_id) REFERENCES filer_image(file_ptr_id) DEFERRABLE INITIALLY DEFERRED
    TABLE "kevlar_site_config_siteconfig" CONSTRAINT "site_logo_id_refs_file_ptr_id_c0419fd8" FOREIGN KEY (site_logo_id) REFERENCES filer_image(file_ptr_id) DEFERRABLE INITIALLY DEFERRED
=======================================================================================================================
CREATE TABLE filer_file (
    id integer NOT NULL,
    folder_id integer,
    file character varying(255),
    _file_size integer,
    has_all_mandatory_data boolean NOT NULL,
    original_filename character varying(255),
    name character varying(255) NOT NULL,
    owner_id integer,
    uploaded_at timestamp with time zone NOT NULL,
    modified_at timestamp with time zone NOT NULL,
    description text,
    is_public boolean NOT NULL,
    sha1 character varying(40) NOT NULL,
    polymorphic_ctype_id integer
);

CREATE TABLE filer_image (
    file_ptr_id integer NOT NULL,
    _height integer,
    _width integer,
    date_taken timestamp with time zone,
    default_alt_text character varying(255),
    default_caption character varying(255),
    author character varying(255),
    must_always_publish_author_credit boolean NOT NULL,
    must_always_publish_copyright boolean NOT NULL,
    subject_location character varying(64)
);
=====================================================================================================================