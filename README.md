
### Pygrametl ETL Importer 

#####1. Setup Aldryn locally. 

      pip install aldryn-client 

#####2. Aldryn doctor

#####3. Aldryn login

     Authenticates you to the Aldryn cloud platform. The login command opens your browser at https://control.aldryn.com/account/desktop-app/access-token/, where you can obtain an access token for identification purposes.

#####4. Update the database information in the config.yml file

     database: "host='localhost' dbname='testimonial' user='kevlar' password='kevlar'" 
--------------------------------------------------------------------------------------------------------------

#####5. Setup the Aldryn project
      aldryn project setup  'NAME-OF-PROJECT'
      aldryn project up

####6. Find the name of the docker container
      docker ps

#####7. To find the IP address of the docker db container to add to the config.yml
      docker inspect <NAME OF CONTAINER>

#####8. Check config.yml in TESTIMONIAL_IMPORTER/ files for necessary parameters. 
Make sure the testimonials and images tsv files are in the data/ folder

#### ONLY FOR DEVELOPMENT MACHINE: NOT FOR PROD

#####10. Login to the db contaianer and delete the tables to start fromn a clean slate
      docker exec -it "NAME OF DB CONTAINER" bash
      psql -U postgres
      delete from kevlar_testimonials_testimonial_translation;
      delete from kevlar_testimonials_testimonial;

#####11. Also Clear The Media Folder '/app/data/media/filer_public/'

      If you have not touched the db or ran any additional db changes then you can 
      run the testimonial importer. Else you will need to do a clean pull on the db.

#####12. Logout of the Db container

#####13. Login to the web container
      sudo docker exec -it 'WEB CONTAINER NAME' bash

#####14. Activate the virtual environment and execute the importer
      Make sure both the containers are up :- db and web
      source tenv/bin/activate
      python TestimonialImporter.py
      

#####15. After the process runs successfully push the db and the media folder.
      aldryn push media, aldryn push db
